## Challenge 1: [Machine Learning](retinopathy_400.ipynb)

## Challenge 2: [Genomic Data Preparation](Genomic_data_preparation.ipynb)

# Kaushik_technical_work

This is a temporary repository to try out two independent projects.
1) Classifier using a demo image data
2) ML on Genomic data


## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/hskaushik/kaushik_technical_work.git
git branch -M main
git push -uf origin main
```

## Dependencies along with versions
- [ ] scikit-learn 0.24.2  
- [ ] OpenCV 4.5.5
- [ ] scikit-image-0.18.3
- [ ] jupyterlab-3.2.9
- [ ] pandas-1.3.5
Or, [retinopathy_env.yml](retinopathy_env.yml) can be used to create the conda environment



